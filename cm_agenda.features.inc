<?php
/**
 * @file
 * cm_agenda.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function cm_agenda_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function cm_agenda_node_info() {
  $items = array(
    'cm_agenda' => array(
      'name' => t('Agenda'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Titel'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
