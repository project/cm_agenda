(function ($) {
  Drupal.behaviors.cm_agenda = {
    attach: function (context, settings) {
      setInterval(googlePing, 30000);
    }
  };

  function googlePing() {
    if (typeof (ga) !== 'undefined' && Drupal.settings.cm_agenda.title != undefined) {
      console.log('Ping');
      ga('send', 'event', 'CM Agenda', 'VOD30s', Drupal.settings.cm_agenda.title + ' [' + Drupal.settings.cm_agenda.fid + ']');
    }
  }

})(jQuery);
