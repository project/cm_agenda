(function ($) {
  Drupal.behaviors.youtube_interactive = {
    attach: function (context, settings) {
      if (Drupal.settings.youtube_interactive.loaded == undefined || !Drupal.settings.youtube_interactive.loaded) {
        Drupal.settings.youtube_interactive.loaded = true;
        Drupal.settings.youtube_interactive.running = false;
        var tag = document.createElement('script');
        tag.src = "https://www.youtube.com/iframe_api";
        tag.addEventListener("load", onYouTubeIframeAPIReady, false);
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
      }
    }
  };

  function onYouTubeIframeAPIReady() {
    if (YT && YT.Player) {
      $.each(Drupal.settings.youtube_interactive.fields, function (index, field) {
        field.lastTime = -1;
        Drupal.settings.youtube_interactive.player = new Array();
        Drupal.settings.youtube_interactive.player[field.fid] = new YT.Player('youtube-interactive-' + field.fid, {
          height: '480',
          width: '854',
          videoId: field.id,
          events: {
            'onReady': onPlayerReady,
          }
        });
        $('[data-youtube-interactive-source*="' + field.fid + '"]').once('js-youtube-interactive-seek', function () {
          var chapter = $(this);
          // jump to the cue point on click
          chapter.on('click', function (e) {
            e.preventDefault();
            var start = parseInt(chapter.data('youtube-interactive-start'));
            Drupal.settings.youtube_interactive.player[field.fid].seekTo(start);
            Drupal.settings.youtube_interactive.player[field.fid].playVideo();
          });
        });
      });
    } else {
      setTimeout(onYouTubeIframeAPIReady, 100);
    }
  }

  function onPlayerReady(event) {
    $.each(Drupal.settings.youtube_interactive.fields, function (index, field) {
      $('#youtube-interactive-' + field.fid).on("timeUpdate", updateEvent);
    });
    checkTime();
    $.each(Drupal.settings.youtube_interactive.fields, function (index, field) {
      Drupal.settings.youtube_interactive.player[field.fid].playVideo();
    });
  }

  function checkTime() {
    $.each(Drupal.settings.youtube_interactive.fields, function (index, field) {
      var currentTime = Math.floor(Drupal.settings.youtube_interactive.player[field.fid].getCurrentTime());
      if (currentTime != field.lastTime) {
        field.lastTime = currentTime;
        $('#youtube-interactive-' + field.fid).trigger('timeUpdate', [field]);
      }
    });
    setTimeout(checkTime, 100);
  }

  function updateEvent(event, field) {
    var l = field.events.length;
    for (var i = 0; i < l; i++) {
      var e = field.events[i];
      if (e.active) {
        if (field.lastTime < e.start || field.lastTime >= e.end) {
          $(e.selector).removeClass(e.class);
          e.active = 0;
        }
      } else {
        if (field.lastTime >= e.start && field.lastTime < e.end) {
          $(e.selector).addClass(e.class);
          e.active = 1;
        }
      }
    }
  }
})(jQuery);
