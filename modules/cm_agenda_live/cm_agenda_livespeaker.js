(function ($) {
  Drupal.behaviors.cm_agenda_livespeaker = {
    attach: function (context, settings) {
      updatePresenter(true);
      setInterval(updatePresenter, 2000);
    }
  };

  function updatePresenter() {
    now = new Date();
    sec = Math.round(now.getTime() / 1000);
    sec = sec - sec % 2;
    $.ajax({
      url: '/ajax/agenda/' + Drupal.settings.cm_agenda_livespeaker.agenda_nid + '?time=' + sec,
      success: function (data) {
        $('.cm-agenda-speaker').html(data.speaker);
        $('.cm-agenda-title').html(data.background_title);
      }
    });
  }
})(jQuery);
