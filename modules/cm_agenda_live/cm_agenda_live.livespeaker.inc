<?php

function cm_agenda_livespeaker($agenda = NULL) {
  $state = cm_agenda_get_state($agenda);
  if ($state == 'restrict') {
    if (!user_access('administer agendas')) {
      drupal_access_denied();
      return;
    }
    drupal_set_message(t('Agenda is not published. Anonymous users will get access denied'), 'warning');
  }
  $settings = array(
    'cm_agenda_livespeaker' => array(
      'agenda_nid' => $agenda->nid,
      'agenda_title' => $agenda->title,
    )
  );
  drupal_add_js($settings, 'setting');
  drupal_add_js(drupal_get_path('module', 'cm_agenda_live') . '/cm_agenda_livespeaker.js');
  return '<div class="cm-agenda-title"></div><div class="cm-agenda-speaker"></div>';
}
